#ifndef STREAM9_NODE_WEAK_NODE_HPP
#define STREAM9_NODE_WEAK_NODE_HPP

#include "shared_node.hpp"

#include <concepts>
#include <memory>
#include <utility>

#include <stream9/null.hpp>
#include <stream9/optional.hpp>

namespace stream9 {

template<typename T>
class weak_node
{
    using base = std::weak_ptr<T>;
public:
    using element_type = base::element_type;

public:
    // essential
    template<typename U>
    weak_node(shared_node<U> const& n)
        requires std::is_constructible_v<base, std::shared_ptr<U> const&>
        : m_base { n.m_base }
    {}

    template<typename U>
    weak_node(weak_node<U> const& o)
        requires (!std::same_as<T, U>)
              && std::is_constructible_v<base, std::weak_ptr<U> const&>
        : m_base { o.m_base }
    {}

    template<typename U>
    weak_node(weak_node<U>&& o)
        requires (!std::same_as<T, U>)
              && std::is_constructible_v<base, std::weak_ptr<U>>
        : m_base { std::move(o.m_base) }
    {}

    weak_node(weak_node const&) = default;
    weak_node& operator=(weak_node const&) = default;

    weak_node(weak_node&&) = default;
    weak_node& operator=(weak_node&&) = default;

    ~weak_node() = default;

    // query
    long use_count() const noexcept { return m_base.use_count(); }
    bool expired() const noexcept { return m_base.expired(); }

    opt<shared_node<T>> lock() const noexcept
    {
        opt<shared_node<T>> r;

        if (auto n = m_base.lock(); n) {
            r = shared_node<T>(n);
        }

        return r;
    }

    template<typename U>
    bool owner_before(weak_node<U> const& other) const noexcept
        requires requires (weak_node n1, weak_node<U> n2) { n1.m_base.owner_before(n2.m_base); }
    {
        return m_base.owner_before(other.m_base);
    }

    template<typename U>
    bool owner_before(shared_node<U> const& other) const noexcept
        requires requires (weak_node n1, shared_node<U> n2) { n1.m_base.owner_before(n2.m_base); }
    {
        return m_base.owner_before(other.m_base);
    }

    // modifier
    void swap(weak_node& o) noexcept
    {
        m_base.swap(o.m_base);
    }

private:
    template<typename U> friend class weak_node;
    template<typename U> friend class shared_node;
    template<typename U> friend class enable_shared_from_this;

    weak_node(base b) : m_base { b } {}

    friend struct null_traits<weak_node<T>>;

    weak_node(null_t) {}
    bool is_null() const noexcept { return m_base.expired(); }
    void set_null() noexcept { m_base.reset(); }

private:
    base m_base; // non-null
};

template<typename T>
void
swap(weak_node<T>& x, weak_node<T>& y) noexcept
{
    x.swap(y);
}

} // namespace stream9

namespace std {

template<typename T>
struct owner_less<stream9::weak_node<T>>
{
    bool
    operator()(stream9::weak_node<T> const& lhs,
               stream9::weak_node<T> const& rhs) const noexcept
    {
        return lhs.owner_before(rhs);
    }

    bool
    operator()(stream9::weak_node<T> const& lhs,
               stream9::shared_node<T> const& rhs) const noexcept
    {
        return lhs.owner_before(rhs);
    }

    bool
    operator()(stream9::shared_node<T> const& lhs,
               stream9::weak_node<T> const& rhs) const noexcept
    {
        return lhs.owner_before(rhs);
    }
};

} // namespace std

#endif // STREAM9_NODE_WEAK_NODE_HPP

