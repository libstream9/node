#ifndef STREAM9_NODE_NODE_HPP
#define STREAM9_NODE_NODE_HPP

#include <concepts>
#include <memory>
#include <utility>

#include <stream9/null.hpp>
#include <stream9/transitive_arrow.hpp>

namespace stream9 {

template<typename T>
class node
{
    using base = std::unique_ptr<T>;
public:
    using element_type = T;
    using pointer = base::pointer;

    static constexpr bool transitive_arrow = true;

public:
    // essential
    template<typename... Args>
    explicit node(Args&&... args)
        requires std::is_constructible_v<T, Args...>
        : m_base { std::make_unique<T>(std::forward<Args>(args)...) }
    {}

    node(node const&) = delete;
    node& operator=(node const&) = delete;

    node(node&&) = default;
    node& operator=(node&&) = default;

    template<typename U>
    node(node<U>&& o)
        requires (!std::same_as<T, U>)
              && std::is_constructible_v<std::unique_ptr<T>, std::unique_ptr<U>>
        : m_base { std::move(o.m_base) }
    {}

    ~node() = default;

    // accessor
    pointer operator->() const noexcept
        requires (!has_transitive_arrow<T>)
    {
        return m_base.get();
    }

    decltype(auto) operator->() const
        noexcept(noexcept(m_base->operator->()))
        requires has_transitive_arrow<T>
    {
        return m_base->operator->();
    }


    T& operator*() const noexcept { return *m_base; }

    pointer operator&() const noexcept { return m_base.get(); }

    // modifier
    void swap(node& o) noexcept { m_base.swap(o.m_base); }

    // conversion
    operator T& () const noexcept { return *m_base; }

    template<typename U>
    operator U () const
        noexcept(noexcept(m_base->operator U()))
        requires requires (T x) { x.operator U(); }
    {
        return m_base->operator U();
    }

    // comparison
    bool operator==(node const& o) const
        noexcept(noexcept(*m_base == *o.m_base))
        requires std::equality_comparable<T>
    {
        return *m_base == *o.m_base;
    }

    template<typename U>
    bool operator==(U const& o) const
        noexcept(noexcept(static_cast<T&>(*this) == o))
        requires std::equality_comparable_with<T, U>
    {
        return static_cast<T&>(*this) == o;
    }

    auto operator<=>(node const& o) const
        noexcept(noexcept(*m_base <=> *o.m_base))
        requires std::three_way_comparable<T>
    {
        return *m_base <=> *o.m_base;
    }

    template<typename U>
    auto operator<=>(U const& o) const
        noexcept(noexcept(static_cast<T&>(*this) <=> o))
        requires std::three_way_comparable_with<T, U>
    {
        return *m_base <=> o;
    }

private:
    template<typename U>
    friend class node;

    friend struct null_traits<node<T>>;

    node(null_t) {}
    bool is_null() const noexcept { return !m_base; }
    void set_null() noexcept { m_base.reset(); }

private:
    base m_base; // non-null
};

template<typename T>
void
swap(node<T>& x, node<T>& y) noexcept
{
    x.swap(y);
}

template<typename T1, typename T2>
T1&
operator<<(T1& x, node<T2> const& y)
    requires requires (T1& x, T2 const& y) { { x << y } -> std::same_as<T1&>; }
{
    return x << *y;
}

} // namespace stream9

#endif // STREAM9_NODE_NODE_HPP
