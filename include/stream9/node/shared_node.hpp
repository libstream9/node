#ifndef STREAM9_NODE_SHARED_NODE_HPP
#define STREAM9_NODE_SHARED_NODE_HPP

#include <concepts>
#include <memory>
#include <utility>

#include <stream9/null.hpp>
#include <stream9/transitive_arrow.hpp>

namespace stream9 {

template<typename T> class weak_node;

template<typename T>
class shared_node
{
    using base = std::shared_ptr<T>;
public:
    using element_type = base::element_type;
    using weak_type = base::weak_type;

    static constexpr bool transitive_arrow = true;

public:
    // essential
    template<typename... Args>
    explicit shared_node(Args&&... args)
        requires std::is_constructible_v<T, Args...>
        : m_base { std::make_shared<T>(std::forward<Args>(args)...) }
    {}

    shared_node(shared_node& o) noexcept
        : shared_node(static_cast<shared_node const&>(o))
    {}

    shared_node(shared_node const&) = default;
    shared_node& operator=(shared_node const&) = default;

    shared_node(shared_node&&) = default;
    shared_node& operator=(shared_node&&) = default;

    template<typename U>
    shared_node(shared_node<U> const& o)
        requires (!std::same_as<T, U>)
              && std::is_constructible_v<std::shared_ptr<T>, std::shared_ptr<U> const&>
        : m_base { o.m_base }
    {}

    template<typename U>
    shared_node(shared_node<U>& o)
        requires (!std::same_as<T, U>)
              && std::is_constructible_v<std::shared_ptr<T>, std::shared_ptr<U>&>
        : m_base { o.m_base }
    {}

    template<typename U>
    shared_node(shared_node<U>&& o)
        requires (!std::same_as<T, U>)
              && std::is_constructible_v<std::shared_ptr<T>, std::shared_ptr<U>>
        : m_base { std::move(o.m_base) }
    {}

    ~shared_node() = default;

    // accessor
    T* operator->() const noexcept
        requires (!has_transitive_arrow<T>)
    {
        return m_base.get();
    }

    decltype(auto) operator->() const
        noexcept(noexcept(m_base->operator->()))
        requires has_transitive_arrow<T>
    {
        return m_base->operator->();
    }

    T& operator*() const noexcept { return *m_base; }

    T* operator&() const noexcept { return m_base.get(); }

    // query
    long use_count() const noexcept { return m_base.use_count(); }

    bool unique() const noexcept { return m_base.unique(); }

    template<typename U>
    bool owner_before(shared_node<U> const& x) const noexcept
    {
        return m_base.owner_before(x.m_base);
    }

    template<typename U>
    bool owner_before(weak_node<U> const& x) const noexcept
    {
        return m_base.owner_before(x.m_base);
    }

    // modifier
    void swap(shared_node& o) noexcept { m_base.swap(o.m_base); }

    // conversion
    operator T& () const noexcept { return *m_base; }

    template<typename U>
    operator U () const
        noexcept(noexcept(m_base->operator U()))
        requires requires (T x) { x.operator U(); }
    {
        return m_base->operator U();
    }

    template<typename U>
    operator std::shared_ptr<U> () const noexcept
        requires std::is_convertible_v<base, std::shared_ptr<U>>
    {
        return m_base;
    }

    template<typename U>
    operator std::weak_ptr<U> () const noexcept
        requires std::is_convertible_v<base, std::weak_ptr<U>>
    {
        return m_base;
    }

    // comparison
    bool operator==(shared_node const& o) const
        noexcept(noexcept(*m_base == *o.m_base))
        requires std::equality_comparable<T>
    {
        return *m_base == *o.m_base;
    }

    template<typename U>
    bool operator==(U const& o) const
        noexcept(noexcept(static_cast<T&>(*this) == o))
        requires std::equality_comparable_with<T, U>
    {
        return static_cast<T&>(*this) == o;
    }

    auto operator<=>(shared_node const& o) const
        noexcept(noexcept(*m_base <=> *o.m_base))
        requires std::three_way_comparable<T>
    {
        return *m_base <=> *o.m_base;
    }

    template<typename U>
    auto operator<=>(U const& o) const
        noexcept(noexcept(static_cast<T&>(*this) <=> o))
        requires std::three_way_comparable_with<T, U>
    {
        return *m_base <=> o;
    }

private:
    template<typename U> friend class shared_node;
    template<typename U> friend class weak_node;
    template<typename U> friend class enable_shared_from_this;

    shared_node(base b) : m_base { b } {}

    friend struct null_traits<shared_node<T>>;

    shared_node(null_t) {}
    bool is_null() const noexcept { return !m_base; }
    void set_null() noexcept { m_base.reset(); }

private:
    base m_base; // non-null
};

template<typename T>
void
swap(shared_node<T>& x, shared_node<T>& y) noexcept
{
    x.swap(y);
}

template<typename T1, typename T2>
T1&
operator<<(T1& x, shared_node<T2> const& y)
    requires requires (T1& x, T2 const& y) { { x << y } -> std::same_as<T1&>; }
{
    return x << *y;
}

template<typename T>
class enable_shared_from_this : public std::enable_shared_from_this<T>
{
    using base = std::enable_shared_from_this<T>;
public:
  shared_node<T>
  shared_from_this()
  {
      return base::shared_from_this();
  }

  shared_node<const T>
  shared_from_this() const
  {
      return base::shared_from_this();
  }

  weak_node<T>
  weak_from_this()
  {
      return base::weak_from_this();
  }

  weak_node<const T>
  weak_from_this() const
  {
      return base::weak_from_this();
  }
};

} // namespace stream9

namespace std {

template<typename T>
struct owner_less<stream9::shared_node<T>>
{
    bool
    operator()(stream9::shared_node<T> const& lhs,
               stream9::shared_node<T> const& rhs) const noexcept
    {
        return lhs.owner_before(rhs);
    }

    bool
    operator()(stream9::shared_node<T> const& lhs,
               stream9::weak_node<T> const& rhs) const noexcept
    {
        return lhs.owner_before(rhs);
    }

    bool
    operator()(stream9::weak_node<T> const& lhs,
               stream9::shared_node<T> const& rhs) const noexcept
    {
        return lhs.owner_before(rhs);
    }
};

} // namespace std

#endif // STREAM9_NODE_SHARED_NODE_HPP
